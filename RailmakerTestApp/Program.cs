﻿using Railmaker;
using System.Diagnostics;

namespace RailmakerTestApp {

    internal class Program {

        private static void Main(string[] args) {
            var curve = new Curve2D();
			curve.ControlPoints.Add(new Point2D(0, 0));
			curve.ControlPoints.Add(new Point2D(1, 0));
			curve.ControlPoints.Add(new Point2D(1, 1));
			Debug.Print(curve.ControlPoints.ToString());
        }

    }

}