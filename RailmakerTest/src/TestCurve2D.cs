﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using NUnit.Framework;
using Railmaker;

namespace RailmakerTest {

    [TestFixture]
    public class TestCurve2D {

        private Curve2D _curve;

        [TestFixtureSetUp]
        public void TestSetUp() {
            _curve = new Curve2D();
            Assert.IsInstanceOf<Curve2D>(_curve);
        }

        [Test]
        public void TestControlPoints() {
            Assert.IsNotNull(_curve.ControlPoints);
            Assert.IsInstanceOf<ChangeableCollection<Point2D>>(_curve.ControlPoints);
        }

        [Test]
        public void TestInterpolatedPoints() {
            Assert.IsNotNull(_curve.InterpolatedPoints);
            _curve.ControlPoints.Add(new Point2D(0, 0));
            _curve.ControlPoints.Add(new Point2D(0, 1));
            Assert.IsNotNull(_curve.InterpolatedPoints[0]);
        }

        [Test]
        public void TestChangedCallback() {
            var cbVerifier = new VerifiesCallback();
            _curve.Changed += cbVerifier.OnChanged;
            Assert.IsFalse(cbVerifier.WasCallbackCalled);
            _curve.ControlPoints.Add(new Point2D());
            Assert.IsTrue(cbVerifier.WasCallbackCalled);
            Assert.IsInstanceOf<ChangeableCollection<Point2D>>(cbVerifier.ObjectInCallback);
        }

        protected class VerifiesCallback : Curve2D {

            internal Object ObjectInCallback { get; set; }

            internal bool WasCallbackCalled { get; set; }

            public override void OnChanged(Object sender, EventArgs e) {
                WasCallbackCalled = true;
                ObjectInCallback = sender;
            }

        }

    }

}