﻿using System;
using NUnit.Framework;
using Railmaker;

namespace RailmakerTest {

    [TestFixture]
    public class TestPoint2D {

        private Point2D point;

        [TestFixtureSetUp]
        public void TestSetUp() {
            point = new Point2D();
            Assert.IsInstanceOf<Point2D>(point);
        }

        [Test]
        public void TestPointPosition() {
            Assert.NotNull(point.Position);
            Assert.IsInstanceOf<Vector2D>(point.Position);
        }

        [Test]
        public void TestChangedCallback() {
            var cbVerifier = new VerifiesCallback();
            point.Changed += cbVerifier.OnChanged;

            point.Position = new Vector2D(0, 0);
            Assert.IsFalse(cbVerifier.WasCallbackCalled);

            point.Position = new Vector2D(0, 1);
            Assert.IsTrue(cbVerifier.WasCallbackCalled);

            Assert.IsInstanceOf<Point2D>(cbVerifier.ObjectInCallback);
        }

        protected class VerifiesCallback : Point2D {

            internal Object ObjectInCallback { get; set; }

            internal bool WasCallbackCalled { get; set; }

            public override void OnChanged(Object sender, EventArgs e) {
                WasCallbackCalled = true;
                ObjectInCallback = sender;
            }

        }

    }

}