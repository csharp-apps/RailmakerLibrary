﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using Railmaker;

namespace RailmakerTest {

    [TestFixture]
    public class TestChangeableCollection {

        [Test]
        public void TestConstructors() {
            var list = new ChangeableCollection<Point2D>();
            Assert.IsInstanceOf<IChangeable>(list);
            Assert.IsInstanceOf<IList<Point2D>>(list);

            var list2 = new ChangeableCollection<IChangeable>(list);
            Assert.IsInstanceOf<IChangeable>(list2);
            Assert.IsInstanceOf<IList<IChangeable>>(list2);
        }

        [Test]
        public void TestGetEnumerator() {
            var list = new ChangeableCollection<IChangeable>();
            Assert.IsNotNull(list.GetEnumerator());
        }

        [Test]
        public void TestAdd() {
            var list = new ChangeableCollection<IChangeable>();
            var cb = new VerifiesCallback();

            list.Changed += cb.OnChanged;
            list.Add(new Point2D());
            Assert.IsTrue(cb.WasCallbackCalled);
        }

        [Test]
        public void TestClear() {
            var list = new ChangeableCollection<IChangeable>();
            var cb = new VerifiesCallback();

            list.Changed += cb.OnChanged;
            list.Clear();
            Assert.IsTrue(cb.WasCallbackCalled);
        }

        [Test]
        public void TestContains() {
            var list = new ChangeableCollection<IChangeable>();
            var point = new Point2D();

            list.Add(point);
            Assert.IsTrue(list.Contains(point));

            list.Remove(point);
            Assert.IsFalse(list.Contains(point));
        }

        [Test]
        public void TestRemove() {
            var cb = new VerifiesCallback();
            var list = new ChangeableCollection<IChangeable>();
            var point = new Point2D();

            list.Add(point);
            Assert.IsTrue(list.Contains(point));

            list.Changed += cb.OnChanged;
            list.Remove(point);
            Assert.IsTrue(cb.WasCallbackCalled);
            Assert.IsFalse(list.Contains(point));
        }

        [Test]
        public void TestCount() {
            var list = new ChangeableCollection<IChangeable>();
            Assert.IsTrue(list.Count == 0);

            list.Add(new Point2D());
            Assert.IsTrue(list.Count == 1);

            list.Add(new Curve2D());
            Assert.IsTrue(list.Count == 2);
        }

        private class VerifiesCallback : ChangeableCollection<IChangeable> {

            private Object ObjectInCallback = false;

            public bool WasCallbackCalled;

            public override void OnChanged(Object sender, EventArgs e) {
                WasCallbackCalled = true;
                ObjectInCallback = sender;
            }

        }

    }

}