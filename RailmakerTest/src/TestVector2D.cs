﻿using NUnit.Framework;
using Railmaker;

namespace RailmakerTest {

    [TestFixture]
    public class TestVector2D {

        [Test]
        public void TestConstructors() {
            var vector = new Vector2D();
            Assert.IsInstanceOf<Vector2D>(vector);

            var vector2 = new Vector2D(5, -5.0);
            Assert.IsInstanceOf<Vector2D>(vector2);
        }

        [Test]
        public void TestAdd() {
            var vector1 = new Vector2D(2, 2);
            var vector2 = new Vector2D(1, 2);

            var vector3 = vector1 + vector2;
            Assert.IsTrue(vector3 == new Vector2D(3, 4));

            var vector4 = Vector2D.Add(vector1, vector2);
            Assert.IsFalse(Vector2D.Equals(vector4, new Vector2D(3, 3)));
            Assert.IsTrue(Vector2D.Equals(vector4, new Vector2D(3, 4)));
        }

        [Test]
        public void TestSubtract() {
            var vector1 = new Vector2D(2, 2);
            var vector2 = new Vector2D(1, 2);

            var vector3 = vector1 - vector2;
            Assert.IsTrue(vector3 == new Vector2D(1, 0));

            var vector4 = Vector2D.Subtract(vector1, vector2);
            Assert.IsFalse(Vector2D.Equals(vector4, new Vector2D(0, 0)));
            Assert.IsTrue(Vector2D.Equals(vector4, new Vector2D(1, 0)));
        }

        [Test]
        public void TestMultiply() {
            var vector = new Vector2D(2, 6.5);

            var vector2 = vector*2;
            Assert.IsTrue(vector2 == new Vector2D(4, 13));

            var vector3 = Vector2D.Multiply(vector, 2);
            Assert.IsTrue(vector3 == new Vector2D(4, 13));
        }

        [Test]
        public void TestDivision() {
            var vector = new Vector2D(3, 5);

            var vector2 = vector/2;
            Assert.IsTrue(vector2 == new Vector2D(1.5, 2.5));

            var vector3 = Vector2D.Divide(vector, 2);
            Assert.IsTrue(vector3 == new Vector2D(1.5, 2.5));
        }

        [Test]
        public void TestDotProduct() {
            var vector1 = new Vector2D(1, 2);
            var vector2 = new Vector2D(4, 0);

            var dotProduct = Vector2D.DotProduct(vector1, vector2);
            Assert.IsTrue(Equals(dotProduct, 4.0));
        }

    }

}