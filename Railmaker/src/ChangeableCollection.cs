﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Railmaker {

    /// <summary>
    /// Represents a changeable list of changeable objects, <br />
    /// which invokes Changed if an item is changed, added or removed.
    /// </summary>
    public class ChangeableCollection<T> : IChangeable, IList<T> where T : IChangeable {

        private List<T> _changeables;

        /// <summary>
        /// Initializes a new ChangeableCollection.
        /// </summary>
        public ChangeableCollection() {
            _changeables = new List<T>();
        }

        /// <summary>
        /// Initializes a new ChangeableCollections.
        /// </summary>
        /// <param name="changeableObjects">Must be an IEnumerable of type IChangeable</param>
        public ChangeableCollection(IEnumerable<T> changeableObjects) {
            if (changeableObjects == null) {
                throw new ArgumentNullException("changeableObjects");
            }
            foreach (var changeable in changeableObjects) {
                changeable.Changed += OnChanged;
                _changeables = new List<T> {
                    changeable
                };
            }
        }

        /// <summary>
        /// Occurs when changed.
        /// </summary>
        public event EventHandler<EventArgs> Changed;

        /// <summary>
        /// Raises the changed event.
        /// </summary>
        public virtual void OnChanged(Object sender, EventArgs e) {
            if (Changed != null) {
                Changed(sender, e);
            }
        }

        /// <summary>
        /// Gets the enumerator.
        /// </summary>
        /// <returns>The enumerator.</returns>
        public IEnumerator<T> GetEnumerator() {
            return _changeables.GetEnumerator();
        }

        /// <summary>
        /// Gets the enumerator.
        /// </summary>
        /// <returns>The enumerator.</returns>
        IEnumerator IEnumerable.GetEnumerator() {
            return GetEnumerator();
        }

        /// <Docs>The item to add to the current collection.</Docs>
        /// <para>Adds an item to the current collection.</para>
        /// <remarks>To be added.</remarks>
        /// <exception cref="System.NotSupportedException">The current collection is read-only.</exception>
        /// <summary>
        /// Add the specified item.
        /// </summary>
        /// <param name="item">Item.</param>
        public void Add(T item) {
            if (item == null) {
                throw new ArgumentNullException("item");
            }
            item.Changed += OnChanged;
            _changeables.Add(item);
            OnChanged(this, EventArgs.Empty);
        }

        /// <summary>
        /// Clear this instance.
        /// </summary>
        public void Clear() {
            UnbindAllEvents();
            _changeables.Clear();
            OnChanged(this, EventArgs.Empty);
        }

        /// <Docs>The object to locate in the current collection.</Docs>
        /// <para>Determines whether the current collection contains a specific value.</para>
        /// <summary>
        /// Contains the specified item.
        /// </summary>
        /// <param name="item">Item.</param>
        public bool Contains(T item) {
            return _changeables.Contains(item);
        }

        /// <summary>
        /// Copies to.
        /// </summary>
        /// <param name="array">Array.</param>
        /// <param name="arrayIndex">Array index.</param>
        public void CopyTo(T[] array, int arrayIndex) {
            throw new NotImplementedException();
        }

        /// <Docs>The item to remove from the current collection.</Docs>
        /// <para>Removes the first occurrence of an item from the current collection.</para>
        /// <summary>
        /// Remove the specified item.
        /// </summary>
        /// <param name="item">Item.</param>
        public bool Remove(T item) {
            if (item == null) {
                throw new ArgumentNullException("item");
            }
            item.Changed -= OnChanged;
            OnChanged(this, EventArgs.Empty);
            return _changeables.Remove(item);
        }

        /// <summary>
        /// Gets the count.
        /// </summary>
        /// <value>The count.</value>
        public int Count {
            get { return _changeables.Count; }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is read only.
        /// </summary>
        /// <value><c>true</c> if this instance is read only; otherwise, <c>false</c>.</value>
        public bool IsReadOnly {
            get { return false; }
        }

        /// <Docs>To be added.</Docs>
        /// <para>Determines the index of a specific item in the current instance.</para>
        /// <summary>
        /// Indexs the of.
        /// </summary>
        /// <returns>The of.</returns>
        /// <param name="item">Item.</param>
        public int IndexOf(T item) {
            return _changeables.IndexOf(item);
        }

        /// <summary>
        /// Insert the specified index and item.
        /// </summary>
        /// <param name="index">Index.</param>
        /// <param name="item">Item.</param>
        public void Insert(int index, T item) {
            _changeables.Insert(index, item);
            OnChanged(this, EventArgs.Empty);
        }

        /// <summary>
        /// Removes at index.
        /// </summary>
        /// <param name="index">Index.</param>
        public void RemoveAt(int index) {
            _changeables.RemoveAt(index);
            OnChanged(this, EventArgs.Empty);
        }

        /// <summary>
        /// Gets or sets the <see cref="IChangeable" /> at the specified index.
        /// </summary>
        /// <param name="index">Index.</param>
        public T this[int index] {
            get { return _changeables[index]; }
            set {
                //if (_changeables[index] == value) {
                //    return;
                //}
                _changeables[index].Changed -= OnChanged;
                _changeables[index] = value;
                _changeables[index].Changed += OnChanged;
                OnChanged(this, EventArgs.Empty);
            }
        }

        private void UnbindAllEvents() {
            foreach (var changeable in _changeables) {
                changeable.Changed -= OnChanged;
            }
        }

    }

}