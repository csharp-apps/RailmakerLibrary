﻿using System;

namespace Railmaker {

    /// <summary>
    /// Point2D.
    /// </summary>
    public class Point2D : IChangeable {

        private Vector2D position;

        /// <summary>
        /// Initializes a new instance of the <see cref="Railmaker.Point2D" /> class.
        /// </summary>
        public Point2D() {
            position = new Vector2D();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Railmaker.Point2D" /> class at the input coordinates.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public Point2D(double x, double y) {
            position = new Vector2D(x, y);
        }

        /// <summary>
        /// Gets or sets the position.
        /// </summary>
        public Vector2D Position {
            get { return position; }
            set {
                if (position == value) {
                    return;
                }
                position = value;
                OnChanged(this, EventArgs.Empty);
            }
        }

        /// <summary>
        /// Occurs when changed.
        /// </summary>
        public event EventHandler<EventArgs> Changed;

        /// <summary>
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnChanged(Object sender, EventArgs e) {
            if (Changed != null) {
                Changed(this, e);
            }
        }

    }

}