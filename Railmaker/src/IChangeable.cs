﻿using System;

namespace Railmaker {

    /// <summary>
    /// IChangeable.
    /// </summary>
    public interface IChangeable {

        /// <summary>
        /// Occurs when changed.
        /// </summary>
        event EventHandler<EventArgs> Changed;

        /// <summary>
        /// This should be linked to any change of the object.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void OnChanged(Object sender, EventArgs e);

    }

}