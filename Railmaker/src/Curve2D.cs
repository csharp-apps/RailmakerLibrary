﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Railmaker {

    /// <summary>
    /// Curve2d.
    /// </summary>
    public class Curve2D : IChangeable {

        /// <summary>
        /// Initializes a new instance of the <see cref="Railmaker.Curve2D" /> class.
        /// </summary>
        public Curve2D() {
            ControlPoints = new ChangeableCollection<Point2D>();
            ControlPoints.Changed += OnChanged;
            InterpolatedPoints = new List<Vector2D>();
        }

        /// <summary>
        /// The points that form the curve.
        /// </summary>
        /// <value>The control points.</value>
        public ChangeableCollection<Point2D> ControlPoints { get; private set; }

        /// <summary>
        /// A list of the points that gets interpolated from the control points.
        /// </summary>
        /// <value>The interpolated points.</value>
        public List<Vector2D> InterpolatedPoints { get; private set; }

        /// <summary>
        /// Event that gets raised whenever this object changes.
        /// </summary>
        public event EventHandler<EventArgs> Changed;

        /// <summary>
        /// Raises the changed event.
        /// </summary>
        public virtual void OnChanged(Object sender, EventArgs e) {
            UpdateInterpolatedPoints();
            if (Changed != null) {
                Changed(sender, e);
            }
        }

        private void UpdateInterpolatedPoints() {
            InterpolatedPoints.Clear();
            InterpolatedPoints.AddRange(ControlPoints.Select(point => point.Position));
        }

    }

}