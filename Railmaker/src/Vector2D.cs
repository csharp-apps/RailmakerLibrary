﻿namespace Railmaker {

    /// <summary>
    /// This class represents a 2-dimensional vector.
    /// </summary>
    public struct Vector2D {

        private double x;

        private double y;

        /// <summary>
        /// Parameter Constructor.
        /// </summary>
        public Vector2D(double x, double y) {
            this.x = x;
            this.y = y;
        }

        /// <summary>
        /// Gets or sets the x coordinate.
        /// </summary>
        public double X {
            get { return x; }
            set { x = value; }
        }

        /// <summary>
        /// Gets or sets the y coordinate.
        /// </summary>
        public double Y {
            get { return y; }
            set { y = value; }
        }

        /// <summary>
        /// Returns true if vectors are equal.
        /// </summary>
        public static bool Equals(Vector2D vector1, Vector2D vector2) {
            return vector1 == vector2;
        }

        /// <summary>
        /// Adds one vector to another.
        /// </summary>
        public static Vector2D Add(Vector2D vector1, Vector2D vector2) {
            return vector1 + vector2;
        }

        /// <summary>
        /// Subtracts one vector from another.
        /// </summary>
        public static Vector2D Subtract(Vector2D vector1, Vector2D vector2) {
            return vector1 - vector2;
        }

        /// <summary>
        /// Multiplies a vector by a number.
        /// </summary>
        public static Vector2D Multiply(Vector2D vector, double number) {
            return vector*number;
        }

        /// <summary>
        /// Divides a vector by a number.
        /// </summary>
        public static Vector2D Divide(Vector2D vector, double number) {
            return vector/number;
        }

        /// <summary>
        /// Returns the dot product of two vectors.
        /// </summary>
        public static double DotProduct(Vector2D vector1, Vector2D vector2) {
            return (vector1.X*vector2.X) + (vector1.Y*vector2.Y);
        }

        #region Overloads

        /// <summary>
        /// Returns true if vectors are equal.
        /// </summary>
        public static bool operator ==(Vector2D vector1, Vector2D vector2) {
            return Equals(vector1.X, vector2.X) && Equals(vector1.Y, vector2.Y);
        }

        /// <summary>
        /// Returns true if vectors are not equal.
        /// </summary>
        public static bool operator !=(Vector2D vector1, Vector2D vector2) {
            return !Equals(vector1.X, vector2.X) || !Equals(vector1.Y, vector2.Y);
        }

        /// <summary>
        /// Adds one vector to another.
        /// </summary>
        public static Vector2D operator +(Vector2D vector1, Vector2D vector2) {
            return new Vector2D(vector1.X + vector2.X, vector1.Y + vector2.Y);
        }

        /// <summary>
        /// Subtracts one vector from another.
        /// </summary>
        public static Vector2D operator -(Vector2D vector1, Vector2D vector2) {
            return new Vector2D(vector1.X - vector2.X, vector1.Y - vector2.Y);
        }

        /// <summary>
        /// Multiplies a vector by a number.
        /// </summary>
        public static Vector2D operator *(Vector2D vector, double number) {
            return new Vector2D(vector.x*number, vector.y*number);
        }

        /// <summary>
        /// Divides a vector by a number.
        /// </summary>
        public static Vector2D operator /(Vector2D vector, double number) {
            return new Vector2D(vector.x/number, vector.y/number);
        }

        #endregion Overloads
    }

}