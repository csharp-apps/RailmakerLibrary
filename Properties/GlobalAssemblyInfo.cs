﻿using System;
using System.Reflection;
using System.Resources;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Railmaker")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("A curve handling library.")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("Railmaker")]
[assembly: AssemblyCopyright("Copyright ©  2015  Victor Glindås")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: NeutralResourcesLanguage("en")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]

[assembly: CLSCompliant(true)]
[assembly: ComVisible(false)]